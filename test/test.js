const FP = require( "../dist/index.js").FP
const applicationXFhirQuery = require( "../dist/index.js").applicationXFhirQuery
const fileTests = require( "@lezer/generator/dist/test").fileTests

const fs = require( "fs")
const path= require( "path")
const fileURLToPath = require( 'url').fileURLToPath;
let caseDir = "./test"

for (let file of fs.readdirSync(caseDir)) {
  if (!/\.fhirpath.txt$/.test(file)) continue

  let name = /^[^\.]*/.exec(file)[0]
  describe(name, () => {
    for (let {name, run} of fileTests(fs.readFileSync(path.join(caseDir, file), "utf8"), file))
      it(name, () => run(FP().language.parser))
  })
}

for (let file of fs.readdirSync(caseDir)) {
  if (!/\.x_fhir_query.txt$/.test(file)) continue

  let name = /^[^\.]*/.exec(file)[0]
  describe(name, () => {
    for (let {name, run} of fileTests(fs.readFileSync(path.join(caseDir, file), "utf8"), file))
      it(name, () => run(applicationXFhirQuery({}).language.parser))
  })
}
