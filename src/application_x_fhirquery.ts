import { parser } from "./application_x_fhirquery.grammar";
import { FPLanguage } from "./fhirpath"
// @ts-ignore
import * as terms from "./application_x_fhirquery.terms"
import {parseMixed} from "@lezer/common"
import {
  Language,
  LRLanguage,
  LanguageSupport,
  indentNodeProp,
  foldNodeProp,
  foldInside,
  delimitedIndent,
} from "@codemirror/language";
import { styleTags, tags as t } from "@codemirror/highlight";

export const applicationXFHIRQueryLanguage = LRLanguage.define({
  parser: parser.configure({
    props: [
      styleTags({
	EXP_SYNT: t.meta
      })
      // indentNodeProp.add({
      //   Application: delimitedIndent({ closing: ")", align: false }),
      // }),
      // foldNodeProp.add({
      //   Application: foldInside,
      // }),
      //       styleTags({
      //         Operator: t.arithmeticOperator,
      //         FunctionExpression: t.function(t.propertyName),
      //         This: t.self,
      //         Variable: t.definition(t.variableName),
      //         Number: t.number,
      //         Identifier: t.name,
      //         Boolean: t.bool,
      //         String: t.string,
      //         LineComment: t.lineComment,
      //         "( )": t.paren,
      //       }),
    ],
  }),
});

/// application-x-fhirquery language support.
export function applicationXFhirQuery(
  config: {
    /// Configuration for a base language
    baseLanguage?: Language | null;
  } = {}
) {
  let  base: Language | undefined;
  if (config.baseLanguage === null) {
  } else if (config.baseLanguage) {
    base = config.baseLanguage;
  } else {
    // Should default to html?
    // let htmlSupport = html({ matchClosingTags: false });
    // support.push(htmlSupport.support);
    // base = htmlSupport.language;
    base = undefined;
  }
  return new LanguageSupport(
    applicationXFHIRQueryLanguage.configure({
      wrap:
        base &&
        parseMixed((node, input) => {
	  // @ts-ignore
	  if (node.type.id === terms.Text){
           return {
             parser: base!.parser,
           };
	  }
	  // @ts-ignore
	  if(node.type.id === terms.Expression) {
	    return {
	      parser: FPLanguage!.parser
	    }
	  }
	  return null
        }),
      top: "Template"
    }),
    []
  );
}
