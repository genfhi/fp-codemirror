import { ExternalTokenizer, ContextTracker } from "@lezer/lr";
// @ts-ignore
import * as tokens from "./application_x_fhirquery.terms";



const symbols = {
  "{": 123,
};

export const textTokens = new ExternalTokenizer((input) => {
  // state means:
  // - 0 nothing matched
  // - 1 '<' matched
  // - 2 '</' + possibly whitespace matched
  // - 3-(1+tag.length) part of the tag matched
  // - lastState whole tag + possibly whitespace matched

  for (let state = 0, matchedLen = 0, i = 0; ; i++) {
    if (input.next < 0) {
      // @ts-ignore
      if (i) input.acceptToken(tokens.Text);
      break;
    }
    if (input.next == symbols["{"] && state === 0) {
      state = 1;
      matchedLen++;
    }
    // Means previous was a bracket
    else if (input.next == symbols["{"] && state === 1) {
      if (matchedLen > 1) {
        input.acceptToken(tokens.Text, -1);
      }
      break;
    } else {
      state = 0;
      matchedLen++;
    }
    input.advance();
  }
});
